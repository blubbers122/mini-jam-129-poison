using System;
using Godot;
using Godot.Collections;

public partial class HUD : CanvasLayer
{
  [Export]
  public TextureProgressBar healthBar;
  [Export]
  public TextureProgressBar thirstBar;
  [Export]
  public ItemList potionBar;
  [Export]
  public Label currentFloorText;
  private int currentFloor = 1;


  private void _on_ninja_hydration_changed(int hydration)
  {
	thirstBar.Value = hydration;
  }


  private void _on_ninja_next_floor()
  {
	currentFloor++;
	currentFloorText.Text = "FLOOR " + currentFloor.ToString();
  }


  private void _on_ninja_health_changed(int health)
  {
	healthBar.Value = health;
  }


  private void _on_ninja_potions_changed(Array<Potion> potions)
  {
	// Replace with function body.
	for (int i = 0; i < potionBar.ItemCount; i++)
	{
	  if (i >= potions.Count)
	  {
		potionBar.SetItemIcon(i, null);

	  }
	  else
	  {
		potionBar.SetItemIcon(i, potions[i].GetNode<Sprite2D>("Sprite2D").Texture);
	  }
	}
  }

}



