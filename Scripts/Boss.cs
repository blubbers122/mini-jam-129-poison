using System;
using Godot;

public partial class Boss : CharacterBody2D, IDamageable
{
  public const float speed = 300.0f;
  [Export] int maxHealth;
  public int health;
  Ninja target;

  public override void _Ready()
  {
	health = maxHealth;
	//TODO: grab ninja scene and target him
  }

  public override void _PhysicsProcess(double delta)
  {
	Vector2 velocity = Velocity;


	Velocity = velocity;
	MoveAndSlide();
  }

  public void TakeDamage(int damage)
  {
	health -= damage;
	if (health <= 0) Die();
  }

  public void Die()
  {

  }
}
