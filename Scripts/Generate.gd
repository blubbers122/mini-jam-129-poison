extends Node2D


@export var level_size = Vector2i(100, 80)
@export var rooms_size = Vector2i(10, 14)
@export var rooms_max : int= 15

@onready var level: TileMap = $TileMap

var start_room : Rect2i
var end_room: Rect2i

func _ready() -> void:
#	_setup_camera()
	_generate()
#	get_node("TileMap/Ninja").Teleport(Vector2i(start_room.position.x + start_room.size.x / 2, start_room.position.y + start_room.size.y / 2) * 16)
	#TODO: spawn enemies in rooms
	
	get_node("TileMap/Goblin").position = Vector2(end_room.position.x + end_room.size.x / 2, end_room.position.y + end_room.size.y / 2) * 16

	var stairs = load("res://Scenes/Stairs.tscn").instantiate()
	get_node("TileMap").add_child(stairs)
	stairs.position = Vector2(end_room.position.x + end_room.size.x / 2, end_room.position.y + end_room.size.y / 2) * 16

func _setup_camera() -> void:
	pass
#	camera.position = level.map_to_local(level_size / 2)
#	var z : int = max(level_size.x, level_size.y) / 8
#	camera.zoom = Vector2(z, z)

func _fill_walls() -> void:
	for x in level_size.x:
		for y in level_size.y:
			level.set_cell(0, Vector2i(x, y), 1, Vector2i(0,2), 0)

func _generate() -> void:
	_fill_walls()
	var data = _generate_data()
	for vector in data:
#		(layer: int, coords: Vector2i, source_id: int = -1, atlas_coords: Vector2i = Vector2i(-1, -1), alternative_tile: int = 0)
		level.set_cell(0, vector, 1, Vector2i(0,3), 0)


func _generate_data() -> Array:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	
	var data := {}
	var rooms := []
	for r in range(rooms_max):
		var room := _get_random_room(rng)
		if _intersects(rooms, room):
			continue
		
		_add_room(data, rooms, room)
		if rooms.size() > 1:
			var room_previous: Rect2 = rooms[-2]
			_add_connection(rng, data, room_previous, room)
	start_room = rooms[rng.randi_range(0, len(rooms)-1)]
	end_room = rooms[rng.randi_range(0, len(rooms)-1)]
	return data.keys()


func _get_random_room(rng: RandomNumberGenerator) -> Rect2:
	var width :int= rng.randi_range(rooms_size.x as int, rooms_size.y as int)
	var height :int= rng.randi_range(rooms_size.x as int, rooms_size.y as int)
	var x :int= rng.randi_range(0, level_size.x as int - width - 1)
	var y :int= rng.randi_range(0, level_size.y as int - height - 1)
	return Rect2(x, y, width, height)


func _add_room(data: Dictionary, rooms: Array, room: Rect2) -> void:
	rooms.push_back(room)
	for x in range(room.position.x+1, room.end.x-1):
		for y in range(room.position.y+1, room.end.y-1):
			data[Vector2(x, y)] = null


func _add_connection(
	rng: RandomNumberGenerator, data: Dictionary, room1: Rect2, room2: Rect2
) -> void:
	var room_center1 : Vector2i= (room1.position + room1.end) / 2
	var room_center2 : Vector2i= (room2.position + room2.end) / 2
	if rng.randi_range(0, 1) == 0:
		_add_corridor(data, room_center1.x, room_center2.x, room_center1.y, Vector2.AXIS_X)
		_add_corridor(data, room_center1.y, room_center2.y, room_center2.x, Vector2.AXIS_Y)
	else:
		_add_corridor(data, room_center1.y, room_center2.y, room_center1.x, Vector2.AXIS_Y)
		_add_corridor(data, room_center1.x, room_center2.x, room_center2.y, Vector2.AXIS_X)


func _add_corridor(data: Dictionary, start: int, end: int, constant: int, axis: int) -> void:
	for t in range(min(start, end), max(start, end) + 1):
		var point := Vector2.ZERO
		match axis:
			Vector2.AXIS_X: point = Vector2(t, constant)
			Vector2.AXIS_Y: point = Vector2(constant, t)
		data[point] = null


func _intersects(rooms: Array, room: Rect2) -> bool:
	var out := false
	for room_other in rooms:
		if room.intersects(room_other):
			out = true
			break
	return out
