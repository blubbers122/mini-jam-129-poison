using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Godot;

public partial class RandomDungeonFloor : Node2D
{
  [Export]
  public TileMap dungeonMap;
  [Export]
  Vector2I floorSize = new Vector2I(100, 80);
  [Export]
  Vector2I roomsSize = new Vector2I(12, 8);
  [Export]
  int maxRooms = 15;
  public Rect2I startRoom, endRoom;
  public List<Rect2I> rooms = new List<Rect2I>();

  public Vector2I wallAtlasCoords = new Vector2I(0, 2);
  public Vector2I floorAtlasCoords = new Vector2I(0, 3);
  public Vector2I stairsAtlasCoords = new Vector2I(2, 6);
  public Vector2I backgroundAtlasCoords = new Vector2I(2, 3);
  public Vector2I doorAtlasCoords = new Vector2I(2, 2);

  // Called when the node enters the scene tree for the first time.
  public override void _Ready()
  {
	GenerateNew();
	// GetNode<Ninja>("TileMap/Ninja").Teleport(new Vector2I(startRoom.Position.X + startRoom.Size.X / 2, startRoom.Position.Y + startRoom.Size.Y / 2) * 16);

  }

  private void ResetFloor()
  {
	for (int x = -20; x < floorSize.X + roomsSize.X + 20; x++)
	{
	  for (int y = -20; y < floorSize.Y + roomsSize.Y + 20; y++)
	  {
		dungeonMap.SetCell(0, new Vector2I(x, y), 1, backgroundAtlasCoords, 0);

	  }
	}
  }

  private void GenerateNew()
  {
	ResetFloor();
	AddRooms();
	AddExit();
	AddEnemies();
	AddPotions();
  }

  private void AddPotions()
  {
	PackedScene potionScene = GD.Load("res://Scenes/Potion.tscn") as PackedScene;
	rooms.ForEach(room =>
	{
	  Random r = new Random();
	  //TODO: factor in current floor
	  if (r.Next(0, 8) == 0)
	  {
		int potionsInRoom = 1;//new Random().Next(1, 3);
		for (int i = 0; i < potionsInRoom; i++)
		{
		  Potion potion = potionScene.Instantiate<Potion>();
		  potion.Position = RandomCellInRoom(room) * 16;
		  dungeonMap.AddChild(potion);
		};
	  }
	});
  }

  private void AddExit()
  {
	dungeonMap.SetCell(0, endRoom.GetCenter(), 1, stairsAtlasCoords, 0);
  }

  private void AddEnemies()
  {
	PackedScene goblinScene = GD.Load("res://Scenes/Goblin.tscn") as PackedScene;

	rooms.ForEach(room =>
	{
	  if (room == startRoom) return;
	  int enemiesInRoom = new Random().Next(1, 4);
	  for (int i = 0; i < enemiesInRoom; i++)
	  {
		Enemy enemy = goblinScene.Instantiate<Enemy>();
		enemy.Position = RandomCellInRoom(room) * 16;
		dungeonMap.AddChild(enemy);
	  }

	});

  }

  private Vector2I RandomCellInRoom(Rect2I room)
  {
	Random rand = new Random();

	return new Vector2I(
	  rand.Next(room.Position.X + 2, room.End.X - 2),
	  rand.Next(room.Position.Y + 2, room.End.Y - 2)
	);
  }

  private void AddRooms()
  {
	Random rng = new Random();
	for (int i = 0; i < maxRooms; i++)
	{

	  Rect2I room = GetRandomRoom(rng);

	  if (rooms.Any<Rect2I>(otherRoom => otherRoom.Intersects(room)))
	  {
		//skipp since we don't want weird jank looking rooms
		continue;
	  }
	  rooms.Add(room);


	  if (rooms.Count > 1)
	  {
		Rect2I previousRoom = rooms[rooms.Count - 2];

		AddRoomConnection(previousRoom, room);
	  }

	  FillRoom(room);


	}
	if (rooms.Count <= 1) throw new Exception("not enough rooms generated");
	startRoom = rooms[rng.Next(0, rooms.Count)];

	endRoom = rooms[rng.Next(0, rooms.Count)];
	while (endRoom == startRoom)
	{
	  endRoom = rooms[rng.Next(0, rooms.Count)];
	}
  }

  private void FillRoom(Rect2I room)
  {
	for (int x = room.Position.X; x <= room.End.X; x++)
	{
	  for (int y = room.Position.Y; y <= room.End.Y; y++)
	  {
		if (x == room.Position.X || x == room.End.X || y == room.Position.Y || y == room.End.Y)
		{
		  if (dungeonMap.GetCellAtlasCoords(0, new Vector2I(x, y)) != floorAtlasCoords)
		  {
			dungeonMap.SetCell(0, new Vector2I(x, y), 1, wallAtlasCoords, 0);
		  }
		}
		else
		{
		  dungeonMap.SetCell(0, new Vector2I(x, y), 1, floorAtlasCoords, 0);
		}
	  }
	}
  }

  private void AddRoomConnection(Rect2I room1, Rect2I room2)
  {
	Random rng = new Random();
	Vector2I room1Center = room1.GetCenter();

	Vector2I room2Center = room2.GetCenter();
	int minX = Math.Min(room1Center.X, room2Center.X), maxX = Math.Max(room1Center.X, room2Center.X);
	int minY = Math.Min(room1Center.Y, room2Center.Y), maxY = Math.Max(room1Center.Y, room2Center.Y);

	if (rng.Next(0, 2) == 0)
	{
	  DrawCorridor(room1Center.X, room2Center.X, room1Center.Y, 0);
	  DrawCorridor(room1Center.Y, room2Center.Y, room2Center.X, 1);
	}
	else
	{
	  DrawCorridor(room1Center.Y, room2Center.Y, room1Center.X, 1);
	  DrawCorridor(room1Center.X, room2Center.X, room2Center.Y, 0);
	}
  }

  private void DrawCorridor(int start, int end, int constant, int axis)
  {
	for (int i = Math.Min(start, end); i < Math.Max(start, end) + 1; i++)
	{
	  Vector2I cell = axis == 0 ? new Vector2I(i, constant) : new Vector2I(constant, i);
	  Vector2I[] neighbors = GetCellNeighbors(cell);
	  foreach (Vector2I neighbor in neighbors)
	  {
		if (dungeonMap.GetCellAtlasCoords(0, neighbor) != floorAtlasCoords)
		{
		  dungeonMap.SetCell(0, neighbor, 1, wallAtlasCoords, 0);
		}
	  }
	  dungeonMap.SetCell(0, cell, 1, floorAtlasCoords, 0);

	}
  }

  private Vector2I[] GetCellNeighbors(Vector2I cell)
  {
	// Define the size of the neighboring points array (9 for a 3x3 grid)
	int numNeighbors = 9;
	// Initialize the neighboring points array
	Vector2I[] neighbors = new Vector2I[numNeighbors];
	// Define the neighboring point offsets
	int index = 0;
	for (int x = -1; x <= 1; x++)
	{
	  for (int y = -1; y <= 1; y++)
	  {
		// Skip the origin point itself
		if (x == cell.X && y == cell.Y) continue;
		// Add the neighboring point offset to the origin point
		Vector2I offset = new Vector2I(x, y);
		Vector2I neighbor = cell + offset;
		// Add the neighboring point to the array
		neighbors[index] = neighbor;
		index++;
	  }
	}
	return neighbors;
  }

  private Rect2I GetRandomRoom(Random rng)
  {
	return new Rect2I(
	  rng.Next(0, floorSize.X),
	  rng.Next(0, floorSize.Y),
	  rng.Next(roomsSize.X / 2, roomsSize.X),
	  rng.Next(roomsSize.Y / 2, roomsSize.Y)
	);
  }

}




