using System;
using Godot;

public partial class Potion : Area2D
{
  [Export]
  Resource potionResource;
  [Export]
  public int hydrationAmount;
  [Export]
  public int healAmount;
  [Export]
  public Color color;
  [Export]
  public int effectDuration;
  //TODO: effect
  // effect duration

  //ideas fire, slow, weakness, poison, thirst

  public void LoadValuesFromPotionResource(string resourcePath)
  {

  }

  public override void _Ready()
  {
  }

  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(double delta)
  {
  }

  // gotta be a much better way to implement this, but doing this for time's sake
  public void ApplyAffect(string affectName, Ninja ninja)
  {
	switch (affectName)
	{
	  case "poison":

		break;
	  case "weakness":
		break;
	  case "thirst":
		break;
	}
  }

  private void _on_body_entered(Node2D body)
  {
	if ((body as Ninja).PickupPotion(this))
	{
	  GetNode<Sprite2D>("Sprite2D").CallDeferred(Sprite2D.MethodName.SetVisible, false);
	  GetNode<CollisionShape2D>("CollisionShape2D").CallDeferred(CollisionShape2D.MethodName.SetDisabled, true);
	}
	//TODO: paraent remove child?
	// QueueFree();
  }
}



