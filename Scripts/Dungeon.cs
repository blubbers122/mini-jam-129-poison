using System;
using Godot;

public partial class Dungeon : Node2D
{
  [Export]
  public int floorCount;
  public int currentFloor;
  [Export]
  AudioStream mainMusic;
  [Export]

  AudioStream bossMusic;
  [Export]
  AudioStreamPlayer audioPlayer;
  Ninja ninja;

  [Signal]
  public delegate void NewFloorNameEventHandler(int floorNum);
  // Called when the node enters the scene tree for the first time.
  public override void _Ready()
  {
	ninja = GetNode<Ninja>("Ninja");
	ninja.GameOver += () => { GameOver(); };
	ninja.NextFloor += () => { NextFloor(); };
	currentFloor = 0;
	NextFloor();
  }

  public void NextFloor()
  {
	GD.Print("generating next floor");
	currentFloor++;

	EmitSignal(SignalName.NewFloorName, currentFloor);

	if (currentFloor == 1)
	{
	  RemoveChild(ninja);
	}
	else
	{
	  RandomDungeonFloor lastFloor = GetNode<RandomDungeonFloor>("DungeonFloor");

	  lastFloor.GetNode("TileMap").CallDeferred(Node.MethodName.RemoveChild, ninja);
	  CallDeferred(Node.MethodName.RemoveChild, lastFloor);
	}

	if (currentFloor == floorCount)
	{
	  LoadBossRoom();
	}
	else
	{
	  LoadRandomRoom();
	}
  }

  //TODO: listen for signal "Game over" and victory

  private void LoadBossRoom()
  {
	audioPlayer.Stream = bossMusic;
	audioPlayer.Play();
	var bossRoom = (GD.Load("res://Scenes/BossRoom.tscn") as PackedScene).Instantiate();
	bossRoom.GetNode("TileMap").CallDeferred(Node.MethodName.AddChild, ninja);
	CallDeferred(Node.MethodName.AddChild, bossRoom);
	ninja.Position = Vector2.Zero;
  }

  private void LoadRandomRoom()
  {

	GD.Print("Loading random room");
	audioPlayer.Stream = mainMusic;
	audioPlayer.Play();
	RandomDungeonFloor dungeonFloor = (GD.Load("res://Scenes/DungeonFloor.tscn") as PackedScene).Instantiate<RandomDungeonFloor>();
	CallDeferred(Node.MethodName.AddChild, dungeonFloor);
	dungeonFloor.GetNode("TileMap").CallDeferred(Node.MethodName.AddChild, ninja);
	dungeonFloor.Ready += () => DungeonReady();
  }

  private void GameOver()
  {
	// currentFloor = 0;
	// NextFloor();
	GD.Print("Game over");
	//TODO: popup saying you lost and leave camera where player died
  }


  public void StairsEntered()
  {
	GD.Print("Stairs used");
	NextFloor();
  }

  public void DungeonReady()
  {
	ninja.Position = GetNode<RandomDungeonFloor>("DungeonFloor").startRoom.GetCenter() * 16;

  }
}

