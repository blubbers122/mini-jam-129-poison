using System;
using System.Collections.Generic;
using Godot;
using Godot.Collections;

public partial class Ninja : CharacterBody2D, IDamageable
{

  [Export]
  public float speed = 6500f;
  [Export]
  public int maxHealth;
  public int health;
  [Export]
  public int maxHydration;
  public int hydration;
  public bool isDead = false;
  public Array<Potion> potions = new Array<Potion>();
  [Export]
  public int maxPotions = 5;
  public bool invulnerable = false;
  [Export]
  public Timer invulnerableTimer;

  [Export]
  public AudioStreamPlayer audioStreamPlayer;
  [Export]
  public AudioStream drinkPotionNoise;
  [Export]
  public AudioStream hurtNoise;
  [Export]
  public AudioStream stepNoise;
  [Export]
  public AnimationTree animationTree;
  public AnimationNodeStateMachinePlayback animationMode;

  [Signal]
  public delegate void GameOverEventHandler();
  [Signal]
  public delegate void NextFloorEventHandler();
  [Signal]
  public delegate void HealthChangedEventHandler(int health);
  [Signal]
  public delegate void HydrationChangedEventHandler(int hydration);
  [Signal]
  public delegate void PotionsChangedEventHandler(Array<Potion> potions);
  [Signal]
  public delegate void PoisonEffectFinishedEventHandler();


  public override void _Ready()
  {
	health = maxHealth;
	hydration = maxHydration;
	animationMode = (AnimationNodeStateMachinePlayback)animationTree.Get("parameters/playback");
	animationMode.Travel("Idle");

  }

  public void TakeDamage(int damage)
  {
	health -= damage;
	EmitSignal(SignalName.HealthChanged, health);
	invulnerable = true;
	invulnerableTimer.Start();
	//TODO: fix not playing hurt noise
	audioStreamPlayer.Stream = hurtNoise;
	audioStreamPlayer.Play();

	if (health <= 0) Die();
  }

  public void Heal(int amount)
  {
	health = Math.Min(maxHealth, health + amount);
	EmitSignal(SignalName.HealthChanged, health);
  }

  public void Hydrate(int amount)
  {
	hydration = Math.Min(maxHydration, hydration + amount);
	EmitSignal(SignalName.HydrationChanged, hydration);
  }

  public void Dehydrate(int amount)
  {
	hydration -= amount;
	EmitSignal(SignalName.HydrationChanged, hydration);
	if (hydration <= 0)
	{
	  Die();
	}
  }

  public void DrinkPotion()
  {
	GD.Print("try drink potion");
	//TODO: allow moving potion selected
	if (potions.Count == 0) return;
	Potion potion = potions[0];
	// // TakeDamage(potion.Damage)
	//apply effect & timer to remove effect after
	Hydrate(potion.hydrationAmount);
	Heal(potion.healAmount);
	audioStreamPlayer.Stream = drinkPotionNoise;
	audioStreamPlayer.Play();
	potions.Remove(potion);
	EmitSignal(SignalName.PotionsChanged, potions);
	GD.Print("drank potion");
	potion.QueueFree();
	//TODO: status effects (speed, poison, strength/weakness, fire)
  }

  public bool PickupPotion(Potion potion)
  {
	if (potions.Count >= 5) return false;
	potions.Add(potion);
	EmitSignal(SignalName.PotionsChanged, potions);
	return true;
  }

  //TODO: throw shuriken


  public override void _PhysicsProcess(double delta)
  {
	Vector2 velocity = Velocity;

	// Get the input direction and handle the movement/deceleration.
	// As good practice, you should replace UI actions with custom gameplay actions.
	Vector2 direction = Input.GetVector("left", "right", "up", "down");


	velocity = direction.Normalized() * speed * (float)delta;//velocity.MoveToward((velocity + direction.Normalized() * speed), acceleration * (float)delta);



	if (velocity == Vector2.Zero)
	{
	  animationMode.Travel("Idle");
	}
	else
	{
	  animationMode.Travel("Walk");
	}

	if (direction != Vector2.Zero)
	{
	  animationTree.Set("parameters/Walk/blend_position", direction);
	  animationTree.Set("parameters/Idle/blend_position", direction);
	  animationTree.Set("parameters/Attack/blend_position", direction);
	}

	//could probably make this better
	if (audioStreamPlayer.Stream == stepNoise)
	{
	  if (velocity == Vector2.Zero)
	  {
		audioStreamPlayer.Stop();
		audioStreamPlayer.Stream = null;
		GD.Print("stop playing step noise");
	  }

	}
	else if (velocity != Vector2.Zero)
	{

	  GD.Print("start playing step noise");
	  audioStreamPlayer.Stream = stepNoise;
	  audioStreamPlayer.Play();
	}


	Velocity = velocity;
	MoveAndSlide();

	if (Input.IsActionJustPressed("attack"))
	{
	  animationMode.Travel("Attack");
	}
	if (Input.IsActionJustPressed("drink_potion")) DrinkPotion();

  }

  private void OnThirstTimerTimeout()
  {
	// Replace with function body.
	Dehydrate(1);

  }

  public void Die()
  {
	isDead = true;
	// Camera2D cam = GetNode<Camera2D>("Camera2D");
	// RemoveChild(cam);
	// cam.GlobalPosition = GlobalPosition;
	// GetNode<Node2D>("/root/Dungeon/DungeonFloor/TileMap").AddChild(cam);
	QueueFree();
	EmitSignal(SignalName.GameOver);
  }

  private void _on_hurt_box_body_entered(Node2D body)
  {
	if (invulnerable) return;
	TakeDamage(1);
  }

  private void _on_invulnerable_timer_timeout()
  {
	invulnerable = false;
  }

  private void _on_stair_sensor_body_entered(Node2D body)
  {
	EmitSignal(SignalName.NextFloor);
  }



}
