using System;
using Godot;

public partial class Enemy : CharacterBody2D, IDamageable
{
  [Export]
  public float speed = 4000.0f;
  [Export]
  public NavigationAgent2D navigationAgent;
  [Export]
  public int maxHealth;
  public int health;

  [Export]
  public Ninja target;
  [Export]
  public AnimationTree animationTree;
  [Export]
  public int maxPathLength;
  public AnimationNodeStateMachinePlayback animationMode;
  public bool stunned = false;
  [Export]
  public Timer stunTimer;
  public Vector2 knockback = Vector2.Zero;

  public override void _Ready()
  {
	health = maxHealth;
	animationMode = (AnimationNodeStateMachinePlayback)animationTree.Get("parameters/playback");
	animationMode.Travel("Walk");

  }

  public void TakeDamage(int damage)
  {
	health -= damage;
	stunTimer.Start();
	stunned = true;
	if (health <= 0) Die();
  }

  public void SetKnockback(Vector2 kb)
  {
	GD.Print("knockback is ", kb);
	knockback = kb;
  }

  public void Die()
  {
	QueueFree();
  }


  public override void _PhysicsProcess(double delta)
  {
	if (target == null || target.isDead) return;
	//TODO: only chase target if we can see them
	navigationAgent.TargetPosition = target.GlobalPosition;
	if (navigationAgent.IsNavigationFinished())
	{
	  //TODO: fight
	  // GD.Print("nav finished");
	  return;
	}

	float moveDelta = speed * (float)delta;
	Vector2 nextPathPosition = navigationAgent.GetNextPathPosition();

	Vector2 currentAgentPosition = GlobalTransform.Origin;

	Vector2 directionToNextPathPosition = (nextPathPosition - currentAgentPosition).Normalized();

	// Vector2 directionToTarget = (navigationAgent.TargetPosition - currentAgentPosition).Normalized();
	if (directionToNextPathPosition != Vector2.Zero)
	{
	  animationTree.Set("parameters/Walk/blend_position", directionToNextPathPosition);

	}

	knockback = knockback.Lerp(Vector2.Zero, .5f);

	Vector2 newVelocity = stunned ? knockback : directionToNextPathPosition * moveDelta;
	navigationAgent.SetVelocity(newVelocity);
	Velocity = newVelocity;
	GD.Print("kb: ", knockback);
	// GD.Print("Next path location", nextPathPosition);
	// GD.Print("velocity", Velocity);
	// GD.Print("current location", GlobalTransform, "target:", navigationAgent.TargetPosition);
	MoveAndSlide();
  }

  private void _on_aggro_range_body_entered(Node2D body)
  {
	GD.Print("see player");
	// if (navigationAgent.GetCurrentNavigationPath().Length > maxPathLength)
	// {
	//   target = null;
	//   return;
	// }
	// else
	// {
	target = body as Ninja;

	// }
  }

  private void _on_aggro_range_body_exited(Node2D body)
  {
	target = null;
	GD.Print("dpn't see player");
  }

  private void _on_hurt_box_area_entered(Area2D area)
  {
	TakeDamage(1);
	SetKnockback((area.Position - Position).Normalized() * 100f);
  }

  private void _on_stun_timer_timeout()
  {
	stunned = false;// Replace with function body.
  }

}











